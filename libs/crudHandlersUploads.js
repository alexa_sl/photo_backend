var mongoose = require('mongoose');
var db = require('./mongoose');

module.exports = function (modelName) {

    var addItem = function (req, res, next) {
        var model = req.body.model;
        var url = req.files.file.path;

        db.model(modelName).find({slug: model}, function (err, data) {
            if (err) next(err);

            db.model('photos').create({
                url: url,
                url_small_300: req.files.file.url_small_300,
                parents: data[0]
            }, function (err) {
                if (err) {
                    next(err);
                } else {
                    res.sendStatus(200);
                }
            });
        });
    };

    return {
        addItem: addItem
    }
};