var mongoose = require('mongoose');
var fs = require('fs');
var path = require('path');
var async = require('async');
var config = require('./config');
var log = require('./log')(module);
var bcrypt = require('bcrypt');
var SALT_WORK_FACTOR = 10;

mongoose.connect('mongodb://' + config.get('mongoose:username') + ':' + config.get('mongoose:password') + '@' + config.get('mongoose:url') + '/' + config.get('mongoose:database'));
var db = mongoose.connection;

db.on('error', function (err) {
    log.error('mongoose error!', err);
});
db.once('open', function callback() {
    log.info('mongoose connected!');
});

var models = {};

//Инициализируем все схемы
var init = function (modelsDirectory, callback) {
    //Считываем список файлов из modelsDirectory
    var schemaList = fs.readdirSync(modelsDirectory);
    //Создаем модели Mongoose и вызываем callback, когда все закончим
    async.each(schemaList, function (item, cb) {
        var modelName = path.basename(item, '.js');
        models[modelName] = require(path.join(modelsDirectory, modelName))(mongoose);
        cb();
    }, callback);
};

//Возвращаем уже созданные модели из списка
var model = function (modelName) {
    var name = modelName.toLowerCase();
    if (typeof models[name] == "undefined") {
        // Если модель на найдена, то создаем ошибку
        throw "Model '" + name + "' is not exist";
    }
    return models[name];
};

var Schema = mongoose.Schema;

// User schema
var User = new Schema({
    username: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    is_admin: { type: Boolean, default: false },
    created: { type: Date, default: Date.now }
});
// Bcrypt middleware on UserSchema
User.pre('save', function(next) {
    var user = this;

    if (!user.isModified('password')) return next();

    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);

        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);
            user.password = hash;
            next();
        });
    });
});

//Password verification
User.methods.comparePassword = function(password, cb) {
    bcrypt.compare(password, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(isMatch);
    });
};
//Define Models
var userModel = mongoose.model('User', User);

module.exports.userModel = userModel;
module.exports.init = init;
module.exports.model = model;