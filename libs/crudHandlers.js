var mongoose = require('mongoose');
var db = require('./mongoose');
var fs = require('fs');

module.exports = function (modelName) {

    var list = function (req, res, next) {
        var date = 1;

        db.model(modelName)
            .find()
            .populate('albumPhotos')
            .sort({_id: date})
            .exec(function (err, data) {
                if (err) next(err);
                res.send({
                    data: data
                });
            })
    };

    var get = function (req, res, next) {
        db.model(modelName)
            .find({slug: req.params.slug})
            .populate('albumPhotos')
            .exec(function (err, data) {
                if (err) next(err);

                if (data) {
                    res.send(data);
                } else {
                    res.status(404);
                }
            })
    };

    var create = function (req, res, next) {
        db.model(modelName).create(req.body, function (err, data) {
            if (err) {
                res.status(400).send(err);
            }
            res.send(data);
        });
    };

    var remove = function (req, res, next) {

        if (req.params.slug) {

            if (modelName == 'albums') {
                db.model(modelName)
                    .find({slug: req.params.slug})
                    .populate('albumPhotos')
                    .exec(function (err, data) {
                        if (err) next(err);

                        data[0].albumPhotos.forEach(function (item) {
                            db.model('photos')
                                .remove({_id: item._id}, function (err) {
                                    if (err) {
                                        next(err);
                                    }
                                });

                            deleteFile(item.url);
                            deleteFile(item.url_small_300);
                        });

                        db.model(modelName).remove({slug: req.params.slug}, function (err, data) {
                            if (err) next(err);
                            res.send(data ? req.params.slug : 404);
                        });
                    });

            } else {
                db.model(modelName).remove({slug: req.params.slug}, function (err, data) {
                    if (err) next(err);
                    res.send(data ? req.params.slug : 404);
                });
            }
        } else if (req.params.url) {

            db.model(modelName)
                .remove({url: req.params.url}, function (err) {
                    if (err) next(err);

                    fs.unlink(req.params.url, function (err) {
                        if (err) {
                            next(err);
                        } else {
                            res.sendStatus(200);
                        }
                    });
                });

            if (req.query.url_small_300) {
                deleteFile(req.query.url_small_300);
            }
        }
    };

    return {
        list: list,
        get: get,
        create: create,
        remove: remove
    }
};

function deleteFile(file) {
    fs.unlink(file, function (err) {
        if (err) {
            next(err);
        }
    });
}