var path = require('path');
var slug = require('mongoose-url-slugs');
var speakingurl = require('speakingurl');

module.exports = function (mongoose) {

    var AlbumSchema = new mongoose.Schema({
        title: {type: String, required: true},
        description: {type: String},
        subDescription: {type: String},
        category: {type: String},
        albumPhotos: [{ type: mongoose.Schema.ObjectId, ref: 'photos'}]
    });

    AlbumSchema.plugin(slug('title', {
        maxLength: 50,
        generator: function (text, separator) {
            text = speakingurl(text);

            var slug = text.toLowerCase().replace(/([^a-z0-9\-\_]+)/g, separator).replace(new RegExp(separator + '{2,}', 'g'), separator);
            if (slug.substr(-1) == separator) {
                slug = slug.substr(0, slug.length - 1);
            }

            return slug;
        }
    }));

    return mongoose.model(path.basename(module.filename, '.js'), AlbumSchema);
};
