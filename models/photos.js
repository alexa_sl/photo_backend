var path = require('path');
var relationship = require('mongoose-relationship');

module.exports = function (mongoose) {

    var PhotoSchema = new mongoose.Schema({
        url: {type: String},
        url_small_300: {type: String},
        parents: [{type: mongoose.Schema.ObjectId, ref: 'albums', childPath: 'albumPhotos'}]
    });

    PhotoSchema.plugin(relationship, { relationshipPathName: 'parents' });

    return mongoose.model(path.basename(module.filename, '.js'), PhotoSchema);
};
