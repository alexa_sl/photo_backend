var jwt = require('express-jwt');
var secret = require('./config/secret');
var routesUsers = require('./routes/users');
var sharp = require('sharp');
var multer = require('multer');
var multerBasic = multer();


var savePhoto = multer({
    dest: './public/uploads/photos/',
    rename: function (fieldname, filename) {
        return filename.replace(/\W+/g, '-').toLowerCase() + Date.now()
    },
    onFileUploadComplete: function (file, req, res) {
        var input = file.path;
        var output = input.substr(0, input.lastIndexOf('.')) || input;
        var newSmall300 = output + '_300.' + file.extension;

        sharp(file.path)
            .resize(300)
            .toFile(newSmall300, function (err) {
            if (err) {
                throw err;
            } else {
                file.url_small_300 = newSmall300;
            }
        });
    }
});

module.exports.setup = function (app, handlers) {
    app.get('/', function (req, res) {
        res.send('Hello, World!')
    });

    app.post('/api/v1/user/register', multerBasic, routesUsers.register);
    app.post('/api/v1/user/signin', routesUsers.signin);
    app.get('/api/v1/user/logout', jwt({secret: secret.secretToken}), routesUsers.logout);

    app.post('/api/v1/uploads', savePhoto, handlers.uploads.addItem);

    app.get('/api/v1/albums', multerBasic, handlers.albums.list);
    app.get('/api/v1/albums/:slug', multerBasic, handlers.albums.get);
    app.post('/api/v1/albums', jwt({secret: secret.secretToken}), handlers.albums.create);
    app.delete('/api/v1/albums/:slug', multerBasic, handlers.albums.remove);

    app.get('/api/v1/photos', multerBasic, handlers.photos.list);
    app.get('/api/v1/photos/:slug', multerBasic, handlers.photos.get);
    app.post('/api/v1/photos', jwt({secret: secret.secretToken}), handlers.photos.create);
    app.delete('/api/v1/photos/:url', multerBasic, handlers.photos.remove);

    app.get('/photos', function (req, res, next) {
        var file = req.query.url;
        res.sendfile(file);
    });
};
