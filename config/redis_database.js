var redis = require('redis');
var redisClient = redis.createClient(6379);
//var redisClient = redis.createClient(6379, 'redis-alexa23-8471190589.redis.irstack.com');
//redisClient.auth("redis-alexa23-8471190589.redis.irstack.com:f327cfe980c971946e80b8e975fbebb4", function(err) {
//    if (err) {
//        throw err;
//    }
//});

redisClient.on('error', function (err) {
    console.log('Error ' + err);
});

redisClient.on('connect', function () {
    console.log('Redis is ready');
});

exports.redis = redis;
exports.redisClient = redisClient;