var express = require('express');
var path = require('path');
var app = express();
var logger = require('morgan');
var methodOverride = require('method-override');
var session = require('express-session');
var bodyParser = require('body-parser');
var db = require('./libs/mongoose');
var log = require('./libs/log')(module);
var RedisStore = require('connect-redis')(session);
var cookieParser = require('cookie-parser');
var routes = require('./routes');

var handlers = {
    albums: require('./handlers/albums'),
    photos: require('./handlers/photos'),
    uploads: require('./handlers/uploads')
};

//CORS middleware
app.all('*', function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Max-Age', '86400');
    res.header('Access-Control-Allow-Headers', 'Authorization,Cache-Control,Origin,Content-Type,Accept');
    res.header('Access-Control-Expose-Headers', 'Origin,Content-Type,Accept,Authorization,X-Set-Authorization');
    //the next() function continues execution and will move onto the requested URL/URI
    next();
});
app.options('*', function(req, res) {
    res.send(200);
});
app.post('/*', function(req, res, next) {
    res.contentType('application/json');
    next();
});

app.use(logger('dev'));
app.use(methodOverride());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(express.static(path.join(__dirname, 'api')));
app.use(cookieParser());
app.use(session({
    secret: 'keyboard cat',
    store: new RedisStore,
    resave: true,
    saveUninitialized: true
}));

function run() {
    routes.setup(app, handlers); // Связуем Handlers с Routes
    db.init(path.join(__dirname, 'models'), function (err, data) {
     app.listen(1445, 'localhost');
    });
}
run();

app.use(function(req, res, next){
    res.status(404);
    log.debug('Not found URL: %s',req.url);
    res.send({ error: 'Not found' });
    return;
});

app.use(function(err, req, res, next){
    console.log(err, 'before');
    res.status(err.status || 500);
    log.error('Internal error(%d): %s',res.statusCode, err.message);
    res.send({ error: err.message });
    return;
});
