var mongoose = require('../libs/mongoose');
var modelName = 'albums';
var handlers = require('../libs/crudHandlers')(modelName);

module.exports = handlers;