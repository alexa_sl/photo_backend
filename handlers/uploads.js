var mongoose = require('../libs/mongoose');
var modelName = 'albums';
var handlers = require('../libs/crudHandlersUploads')(modelName);

module.exports = handlers;